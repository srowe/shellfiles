if [[ -n "$PS1" ]]
then
    . ${HOME}/.bashrc
    check_screen
    [[ -z "${TMUX}" ]] && check_tmux
else
    #source ${HOME}/.bash_profile
    OS=`uname -s`;
    export PATH=${HOME}/bin:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin:/usr/X11R6/bin:/opt/TWWfsw/bin:/usr/local/sbin
    export EDITOR=vi
    export VISUAL=vi
    export PAGER=less
    export LESS=-MecX
    function lis () { source ~/.bashrc ; } ;
    [ -f ${HOME}/.bash-local/env ] && . ${HOME}/.bash-local/env
    [ -f ${HOME}/.bash-local/aliases ] && . ${HOME}/.bash-local/aliases
    [ -f ${HOME}/.bash-local/func ] && . ${HOME}/.bash-local/func
fi

# Created by `pipx` on 2024-10-29 19:50:05
export PATH="$PATH:/home/sam.rowe/.local/bin"
