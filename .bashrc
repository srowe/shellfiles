[[ -z "${PS1}" ]] && return
[[ -f ${HOME}/.bash-local/init ]] && source ${HOME}/.bash-local/init
source ${HOME}/.bash/env
source ${HOME}/.bash/aliases
source ${HOME}/.bash/func
[[ -n "${PS1}" ]] && source ${HOME}/.bash/prompt
[[ -n ${NEWSAGENT} ]] && grabssh
updateAgent
